export interface userInfor{
    User_Information:{
        data:{
            Anh: string,
            Email: string,
            Ho: string, 
            NgaySinh: string,
            PhanQuyen:string,
            SDT: number,
            Ten: string,
            TenDangNhap: string,
        },
        id:string,
    }
    status: 'loading' | 'finished' | 'error';
}

