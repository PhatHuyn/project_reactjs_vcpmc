export interface userAuth{
    user_Auth:
    {
        username: string,
        password: string,
        remember?: boolean,
        isLogin: boolean,
    },
    status: 'loading' | 'finished' | 'error';
}

export interface valuesSubmit{
    username: string,
    password: string,
    remember: boolean,
}