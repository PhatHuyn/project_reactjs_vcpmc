export interface DataTableType{
    key: string,
    name: string,
    age: number,
    address:string,
}