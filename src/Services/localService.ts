const USER = "USER";
const USER_REMEMBER = "USER_REMEMBER";
const USER_INFOR = "USER_INFOR";
const USER_LOGIN = "USER_LOGIN";


export const localServ = {
  User: {
    set: (data:any) => {
      let jsonData = JSON.stringify(data);
      localStorage.setItem(USER, jsonData);
    },
    get: () => {
      let jsonData = localStorage.getItem(USER);
      if (jsonData) {
        return JSON.parse(jsonData);
      } else {
        return null;
      }
    },
    remove: () => {
      localStorage.removeItem(USER);
    },
  },
  UserLogin: {
    set: (data:any) => {
      let jsonData = JSON.stringify(data);
      localStorage.setItem(USER_LOGIN, jsonData);
    },
    get: () => {
      let jsonData = localStorage.getItem(USER_LOGIN);
      if (jsonData) {
        return JSON.parse(jsonData);
      } else {
        return null;
      }
    },
    remove: () => {
      localStorage.removeItem(USER_LOGIN);
    },
  },
  UserRemember: {
    set: (data:any) => {
      let jsonData = JSON.stringify(data);
      localStorage.setItem(USER_REMEMBER, jsonData);
    },
    get: () => {
      let jsonData = localStorage.getItem(USER_REMEMBER);
      if (jsonData) {
        return JSON.parse(jsonData);
      } else {
        return null;
      }
    },
    remove: () => {
      localStorage.removeItem(USER_REMEMBER);
    },
  },
  UserInfor: {
    set: (data:any) => {
      let jsonData = JSON.stringify(data);
      localStorage.setItem(USER_INFOR, jsonData);
    },
    get: () => {
      let jsonData = localStorage.getItem(USER_INFOR);
      if (jsonData) {
        return JSON.parse(jsonData);
      } else {
        return null;
      }
    },
    remove: () => {
      localStorage.removeItem(USER_INFOR);
    },
  }
};
