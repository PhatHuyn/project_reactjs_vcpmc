import {configureStore} from '@reduxjs/toolkit'
import userInforReducer from './User/userInforSlice'
import userAuthReducer from './User/userAuthSlice'
import adminMenuReducer from './AdminMenu/adminMenuSlice'
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';


const store = configureStore({
    reducer: {
        userInfor: userInforReducer,
        userAuth: userAuthReducer,
        adminMenu: adminMenuReducer
    },
});

export default store;

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>

export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;