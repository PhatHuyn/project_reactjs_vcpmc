import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import { userAuth } from '../../Interface/User/userAuth';

const initialState :userAuth = {
    user_Auth:{
        username: "",
        password: "",
        isLogin: false,
    },
    status:"loading",
}

// export const SignInUser = createAsyncThunk("user/SignInUser", 
//     async(obbUser: any) => { 
//         const {username, password, remember} = obbUser;
//         let user;
//         let userLogin;
//         let userAuth_login;
//         let isLogin = false;
//         await signInWithEmailAndPassword(auth, username, password)
//         .then((res) => {
//             userAuth_login = { username, password, remember, isLogin }
//             // lấy dữ liệu user login
//             user = res.user;
//             alert("Đăng nhập thành công!!!");
//             localServ.User.set(user);
//             isLogin = true;
//             // Lưu remember
//             if (remember != undefined && remember == true) {
//                 localServ.UserRemember.set(userAuth_login)
//                 userLogin = userAuth_login;
//             } else if (remember == undefined) {
//                 const userAuth_login2 = { username, password, isLogin };
//                 userLogin = userAuth_login2;
//             } else if (remember == false) {
//                 localServ.UserRemember.remove();
//                 userLogin = userAuth_login;
//             }
//         })
//         .catch((error) => { 
//             const errcode = error.code;
//             const errorMessage = error.message;
//             console.log(errcode + errorMessage);
//         })
//         return userLogin;
//     }        
// )

export const userAuthSlice = createSlice({
    name: "user",
    initialState,
    reducers:{
        SignOutUser: (state, action) => {
            state.user_Auth = {username:"", password:"", isLogin:false};
            state.status = "loading";
        },
        UserLogin: (state, action) => { 
            state.user_Auth = action.payload;
            state.status = "finished";
         }
    },
    // extraReducers: (builder) => {
    //     builder.addCase(SignInUser.fulfilled, (state:any , action) => { 
    //         state.user_Auth = action.payload;
    //         state.status = "finished";
    //     })
    // }
});

export const {
    SignOutUser,
    UserLogin
 } = userAuthSlice.actions;

export default userAuthSlice.reducer;