import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {  userInforCollectionRef } from '../../Firebase/init_Firebase';
import { getDocs } from 'firebase/firestore';
import { localServ } from '../../Services/localService';
import { userInfor } from '../../Interface/User/userInfor';

const initialState :userInfor = {
    User_Information:{
        data:{
            Anh: "",
            Email: "",
            Ho: "",
            NgaySinh: "",
            PhanQuyen: "",
            SDT: 0,
            Ten: "",
            TenDangNhap: ""
        },
        id: "",
    },
    status: 'loading',
}

export const GetUserInfor = createAsyncThunk("user/GetUserInfor", 
    async() => { 
        //Lấy Email là local
        let userEmailLogin = localServ.User.get()?.email;
        //Call dữ liệu tù firebase
        const resultUserInfor = await getDocs(userInforCollectionRef);
        const userInfor = resultUserInfor.docs.map((doc) => ({
            data: doc.data(),
            id: doc.id,
        }));
        // So sánh dữ liệu firebase và email local để lấy dữ liệu email đăng nhập
        let dataUserInfor;
        userInfor.forEach(uf => {
            if(userEmailLogin == uf.data.Email){
                dataUserInfor = uf;
            }
        });
        localServ.UserInfor.set(dataUserInfor);
        return dataUserInfor;
    }
)

export const userInforSlice = createSlice({
    name: "userInfor",
    initialState,
    reducers:{},
    extraReducers: (builder) => {
        builder.addCase(GetUserInfor.fulfilled, (state:any , action) => { 
            state.User_Information = action.payload;
            state.status = "finished";
        })
    },
});

export default userInforSlice.reducer;