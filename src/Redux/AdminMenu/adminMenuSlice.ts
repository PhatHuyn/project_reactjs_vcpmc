import { createSlice} from '@reduxjs/toolkit'
import { AdminMenu } from '../../Interface/AdminMenu/adminMenu';

const initialState :AdminMenu = {
    menu: false,
    contract: true,
}

export const adminMenuSlice = createSlice({
    name: "AdminMenu",
    initialState,
    reducers:{
        setAdminMenu: (state, action) => {
            state.menu = action.payload;
        },
        setContract: (state, action) => {
            state.contract = action.payload;
        }
    },
});

export const {
    setAdminMenu,
    setContract
 } = adminMenuSlice.actions;

export default adminMenuSlice.reducer;