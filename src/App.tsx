import './App.css';
import "antd/dist/reset.css";
import AppRouter from './Component/AppRouter/AppRouter';

function App() {
  return (
    <div className="App">
      <AppRouter />
    </div>
  );
}

export default App;
