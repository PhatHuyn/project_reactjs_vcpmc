import React from 'react'
import type { MenuProps } from 'antd';
import { Button, Dropdown, Space } from 'antd';
import logoVN from '../../assets/images/logo_VN.png'
import logoEN from '../../assets/images/logo_English.png'
import { AiOutlineDown } from 'react-icons/ai'

interface LoginLayoutProps {
    Component: React.ReactElement
}

const LoginLayout = (props: LoginLayoutProps) => {

    const handleMenuClick: MenuProps['onClick'] = (e) => {
        console.log('click', e);
    };

    const items: MenuProps['items'] = [
        {
            label: "Tiếng việt",
            key: '1',
        },
        {
            label: "English",
            key: '2',
        },
    ];

    const menuProps = {
        items,
        onClick: handleMenuClick,
    };
    const menuDropDown = () => {
        return (
            <Dropdown menu={menuProps}>
                <Button>
                    <Space>
                        Tiếng việt
                        <span className='Dropdown_logo' >
                            <img src={logoVN} alt="logo VN" />
                        </span>
                        <AiOutlineDown />
                    </Space>
                </Button>
            </Dropdown>
        )
    }

    return (
        <div>
            <div className='HeaderLoginLayout'>
                <div className='HeaderLogin_menu_dropdown'>
                    {menuDropDown()}
                </div>
            </div>
            {props.Component}
        </div>
    )
}

export default LoginLayout