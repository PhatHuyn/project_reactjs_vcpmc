import React, { useEffect } from 'react'
import HeaderMenuPage from '../../Component/HeaderPage/HeaderMenuPage'
import { useAppDispatch } from '../../Redux/store';
import { GetUserInfor } from '../../Redux/User/userInforSlice';

interface HeaderProps {
    Component: React.ReactElement
}

const HeaderLayout = (props: HeaderProps) => {
    const dispatch = useAppDispatch();

    // Get data user
    useEffect(() => {
        dispatch(GetUserInfor());
    }, [])

    return (
        <div>
            <HeaderMenuPage Component={props.Component} />
        </div>
    )
}

export default HeaderLayout
