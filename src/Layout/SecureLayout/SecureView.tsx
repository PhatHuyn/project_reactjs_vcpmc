import React, { useEffect, useState } from 'react'
import { onAuthStateChanged } from 'firebase/auth';
import { auth } from '../../Firebase/init_Firebase';

interface SecureViewProps {
    children: React.ReactElement
}

const SecureView = (props: SecureViewProps) => {
    const [user2, setuser2] = useState(false)
    useEffect(() => {
        checkAuth();
    }, [])
    // Check User tồn tại chưa
    const checkAuth = async () => {
        onAuthStateChanged(auth, user => {
            if (!user) {
                window.location.href = "/login";
            } else {
                setuser2(true);
            }
        })
    }

    return (
        user2 ? (
            <div>{props.children}</div>
        ) : (
            <>
            </>
        )
    )
}

export default SecureView