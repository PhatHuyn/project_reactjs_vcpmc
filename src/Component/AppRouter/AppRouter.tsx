import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import LoginPage from '../AuthUserPage/LoginPage'
import HeaderLayout from '../../Layout/HeaderLayout/HeaderLayout'
import SecureView from '../../Layout/SecureLayout/SecureView'
import ResetPassPage from '../AuthUserPage/ResetPassPage'
import LoginLayout from '../../Layout/LoginLayout/LoginLayout'
import NotFoundPage from '../404/NotFoundPage'
import ForgotPassPage from '../AuthUserPage/ForgotPassPage'
import AdminUserInformation from '../AdminPage/User/AdminUserInformation'
import AdminPage from '../AdminPage/AdminPage'
import AdminRecordStore from '../AdminPage/RecordStore/AdminRecordStore'
import AdminContractManagement from '../AdminPage/ContractManagement/AdminContractManagement'


const AppRouter = () => {
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<LoginLayout Component={<LoginPage />} />} />
                    <Route path='/Login' element={<LoginLayout Component={<LoginPage />} />} />
                    <Route path='/forgotPass' element={<LoginLayout Component={<ForgotPassPage />} />} />
                    <Route path='/resetPass' element={<LoginLayout Component={<ResetPassPage />} />} />


                    <Route path='/Admin/AdminUserInformation' element={<SecureView>
                        <HeaderLayout Component={<AdminUserInformation />} />
                    </SecureView>} />
                    <Route path='/Admin/AdminRecordStore' element={<SecureView>
                        <HeaderLayout Component={<AdminRecordStore />} />
                    </SecureView>} />
                    <Route path='/Admin/AdminContractManagement' element={<SecureView>
                        <HeaderLayout Component={<AdminContractManagement />} />
                    </SecureView>} />
                    <Route path='*' element={<LoginLayout Component={<NotFoundPage />} />} />
                </Routes>
            </BrowserRouter>
        </>
    )
}

export default AppRouter