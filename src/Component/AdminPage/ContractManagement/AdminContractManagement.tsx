import React, { useEffect, useState } from 'react'
import { AppDispatch, RootState, useAppSelector } from '../../../Redux/store';
import { Breadcrumb } from 'antd';
import AuthorizationContract from './AuthorizationContract';
import MiningContract from './MiningContract';
import { useDispatch } from 'react-redux';
import { setContract } from '../../../Redux/AdminMenu/adminMenuSlice';

const AdminContractManagement = () => {
    const dispatch = useDispatch<AppDispatch>();
    const menu = useAppSelector(
        (state: RootState) => state.adminMenu.menu
    );
    const contract = useAppSelector(
        (state: RootState) => state.adminMenu.contract
    );

    // UserClick
    useEffect(() => {
        handleOnClick()
    }, [])

    const handleOnClick = () => {
        let list = document.querySelectorAll<HTMLElement>(".AdminContractManagement_Switch")
        for (let i = 0; i <= list.length; i++) {
            if (list[i] != undefined) {
                console.log('list[i]: ', list[i]);
                list[i].onclick = function () {
                    let j = 0;
                    while (j < list.length) {
                        list[j++].className = 'AdminContractManagement_Switch';
                    }
                    list[i].className = 'AdminContractManagement_Switch active';
                    let id = list[i].id;
                    if (id == "UQ") {
                        dispatch(setContract(true));
                    } else if (id == "KT") {
                        dispatch(setContract(false));
                    }
                }
            } else {
                return;
            }
        }
    }


    return (
        <div className='AdminContractManagements'>
            {menu ? (
                <Breadcrumb style={{ padding: '0px 60px 4px' }}
                    separator=">"
                    items={[
                        {
                            title: 'Quản lý',
                        },
                        {
                            title: 'Quản lý hợp đồng',
                        }
                    ]}
                />
            ) : (<></>)}
            <div className='AdminContractManagements_Content'>
                {contract ? (<>
                    <h2 className='text-3xl'>Danh sách hợp đồng</h2>
                </>) : (<>
                    <h2 className='text-3xl'>Danh sách hợp đồng khai thác</h2>
                </>)}
                <div className='AdminContractManagement_Switch_Admin'>
                    <div className='AdminContractManagement_Switch active' id='UQ'>
                        <div className='AdminContractManagement_Switch_UQ'>Hợp đồng uỷ quyền</div>
                    </div>
                    <div className='AdminContractManagement_Switch' id='KT'>
                        <div className='AdminContractManagement_Switch_KT'>Hợp đồng khai thác</div>
                    </div>
                </div>
                <div>
                    {contract ? (<>
                        <AuthorizationContract />
                    </>) : (<>
                        <MiningContract />
                    </>)}
                </div>
            </div>
        </div>
    )
}

export default AdminContractManagement