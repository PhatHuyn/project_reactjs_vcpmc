import React, { useState } from 'react'
import { Col, Row, Select, Input, Table, Pagination } from 'antd';
import type { ColumnsType, TablePaginationConfig } from 'antd/es/table';
import { DataTableType } from '../../../Interface/AdminContract/adminContract';
const { Search } = Input;

const AuthorizationContract = () => {

    const handleChange = (value: string) => {
        console.log(`selected ${value}`);
    };


    const onSearch = (value: string) => console.log(value);

    const columns: ColumnsType<DataTableType> = [
        {
            title: 'Key',
            dataIndex: 'key',
            width: '20%',
        },
        {
            title: 'Name',
            dataIndex: 'name',
            width: '20%',
        },
        {
            title: 'Age',
            dataIndex: 'age',
            width: '20%',
        },
        {
            title: 'Address',
            dataIndex: 'address',
        },
    ];
    const data: DataTableType[] = [
        {
            key: '1',
            name: 'John Brown',
            age: 32,
            address: 'New York No. 1 Lake Park',
        },
        {
            key: '2',
            name: 'Jim Green',
            age: 42,
            address: 'London No. 1 Lake Park',
        },
        {
            key: '3',
            name: 'Joe Black',
            age: 32,
            address: 'Sydney No. 1 Lake Park',
        },
        {
            key: '4',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '5',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '6',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '7',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '8',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '9',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '10',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '11',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '12',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '13',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '14',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '15',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '16',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
        {
            key: '17',
            name: 'Jim Red',
            age: 32,
            address: 'London No. 2 Lake Park',
        },
    ];

    return (
        <div className='AuthorizationContract'>
            <div className='AuthorizationContract_Setting'>
                <Row>
                    <Col span={6}>
                        <div className='AuthorizationContract_Setting_Select'>
                            <p>Quyền sở hữu:</p>
                            <Select
                                defaultValue="lucy"
                                style={{ width: 120 }}
                                onChange={handleChange}
                                options={[
                                    { value: 'jack', label: 'Jack' },
                                    { value: 'lucy', label: 'Lucy' },
                                ]}
                            />
                        </div>
                    </Col>
                    <Col span={6}>
                        <div className='AuthorizationContract_Setting_Select'>
                            <p>Hiệu lực hợp đồng:</p>
                            <Select
                                defaultValue="lucy"
                                style={{ width: 120 }}
                                onChange={handleChange}
                                options={[
                                    { value: 'jack', label: 'Jack' },
                                    { value: 'lucy', label: 'Lucy' },
                                ]}
                            />
                        </div>
                    </Col>
                    <Col span={12}>
                        <div className='Search'>
                            <Search placeholder="input search text" onSearch={onSearch} style={{ width: 200 }} />
                        </div>
                    </Col>
                </Row>
            </div>
            <div className='AuthorizationContract_list'>
                <Table
                    columns={columns}
                    rowKey={(record) => record.key}
                    dataSource={data}
                    pagination={{ pageSize: 5, total: 13, showTotal: (total) => `Total ${total} items` }}
                />
            </div>
        </div>
    )
}

export default AuthorizationContract