import React, { useEffect, useState } from 'react'
import { Avatar } from 'antd'
import { Form, Input, Col, Row, DatePicker } from 'antd';
import type { FormItemProps } from 'antd';
import { BsCamera, BsCheckCircle } from 'react-icons/bs'
import { FiEdit, FiLock, FiLogOut } from 'react-icons/fi'
import { signOut, updatePassword } from 'firebase/auth';
import { auth, db } from '../../../Firebase/init_Firebase';
import { useNavigate } from 'react-router';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../../Redux/store';
import { localServ } from '../../../Services/localService';
import { SignOutUser, UserLogin } from '../../../Redux/User/userAuthSlice';
import dayjs from 'dayjs';
import { doc, updateDoc } from 'firebase/firestore';
import moment from "moment";
import { GetUserInfor } from '../../../Redux/User/userInforSlice';

const AdminUserInformation: React.FC = () => {
    let navigate = useNavigate();
    const dispatch = useDispatch<AppDispatch>();
    // data UserInfor Local
    const user_infor_local = localServ.UserInfor.get();
    // Lấy pass của user trong local
    const passUserNow = localServ.UserLogin.get()?.password

    // UseState
    const dayFormat = "DD/MM/YYYY";
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isNotifi, setIsisNotifi] = useState(false);
    const [error, seterror] = useState(false);
    const [error2, seterror2] = useState("");
    const [editModal, seteditModal] = useState(false);
    const [days, setdays] = useState(`${user_infor_local.data?.NgaySinh}`);

    // UserClick 
    useEffect(() => {
        const userclick = document.querySelectorAll<HTMLElement>(".AdminUserInfor_Setting_item")
        userclick.forEach(userClick => {
            if (userClick != undefined) {
                const idClick = userClick.id
                userClick.addEventListener("click", function () {
                    if (idClick == "Edit_UserInfor") {
                        const userclick = document.querySelectorAll<HTMLElement>(".AdminUserInfor_Setting");
                        userclick[0].style.display = "none";
                        seteditModal(true);
                    } else if (idClick == "ResetPass_UserInfor") {
                        showModal();
                    } else if (idClick == "Logout_UserInfor") {
                        signOutUser();
                    }
                })
            } else {
                return;
            }
        });
    }, [])

    //Thư viện hỗ trợ
    const MyFormItemContext = React.createContext<(string | number)[]>([]);
    function toArr(str: string | number | (string | number)[]): (string | number)[] {
        return Array.isArray(str) ? str : [str];
    }
    const MyFormItem = ({ name, ...props }: FormItemProps) => {
        const prefixPath = React.useContext(MyFormItemContext);
        const concatName = name !== undefined ? [...prefixPath, ...toArr(name)] : undefined;

        return <Form.Item name={concatName} {...props} />;
    };
    const validateMessages = {
        required: '${label} không được bỏ trống!',
        types: {
            email: '${label} phải là dạng email',
            number: '${label} is not a valid number!',
        },
        number: {
            range: '${label} must be between ${min} and ${max}',
        },
    };

    // Đăng Xuất
    const signOutUser = () => {
        signOut(auth);
        dispatch(SignOutUser);
        localServ.User.remove();
        localServ.UserInfor.remove();
        localServ.UserLogin.remove();
        alert("Đăng xuất thành công");
        navigate("/Login");
    }
    // ResetPass
    const handleResetPass = (password: string) => {
        const user: any = auth.currentUser;
        updatePassword(user, password).then(() => {
            // Update successful.
            const username = localServ.UserLogin.get()?.username;
            const remember = localServ.UserRemember.get()?.remember;
            const User = { password, username, remember };
            setIsModalOpen(false);
            setIsisNotifi(true);
            setTimeout(() => {
                setIsisNotifi(false);
            }, 800)
            // Kiểm tra remember Local và set lại mk bên local
            if (remember == true || remember != undefined) {
                localServ.UserRemember.set(User);
                localServ.UserLogin.set(User);
                dispatch(UserLogin(User));
            } else if (remember == undefined) {
                const User2 = { username, password };
                localServ.UserLogin.set(User2);
                dispatch(UserLogin(User2));
            }
        }).catch((error) => {
            alert("đổi pass thất bại")
        });
    }
    // EditUser
    const handleEdit = async (ho: string, ten: string, ngaySinh: string, sdt: number) => {
        let id = user_infor_local?.id;
        const docRef = doc(db, "UserInformation", id);
        await updateDoc(docRef, { Ho: ho, Ten: ten, NgaySinh: ngaySinh, SDT: sdt })
            .then(async (res) => {
                console.log(res);
                const userclick = document.querySelectorAll<HTMLElement>(".AdminUserInfor_Setting");
                userclick[0].style.display = "block";
                await seteditModal(false);
                alert("Cập nhật thành công!");
                dispatch(GetUserInfor());
            })
            .catch((err) => {
                console.log(err.massage);
            });
    }

    const onFinishUserInfor = (values: any) => {
        console.log('values: ', values);
        let ho = values?.Ho;
        let ten = values?.Ten;
        let ngaySinh = values?.NgaySinh;
        let sdt = values?.SDT;
        if (ngaySinh == undefined) {
            ngaySinh = days;
            handleEdit(ho, ten, ngaySinh, sdt);
        } else {
            let date = ngaySinh.toISOString();
            let dateString = moment(date).format(dayFormat)
            handleEdit(ho, ten, dateString, sdt);
        }
    };
    const onFinishUserResetPass = (values: any) => {
        const passnow = values.passwordNow;
        const passnew = values.passwordNew;
        const passnew2 = values.passwordNew2;
        if (passnow == passUserNow) {
            seterror(false);
            if (passnew == passnew2) {
                seterror(false);
                handleResetPass(passnew);
            }
            else {
                seterror(true);
                seterror2("Mật khẩu mới chưa khớp")
            }
        } else {
            seterror(true);
            seterror2("Mật khẩu hiện tại không chính xác")
        }
    };
    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    const showModal = () => {
        setIsModalOpen(true);
    };
    const handleCancel = () => {
        setIsModalOpen(false);
    };

    return (
        <div className='AdminUserInfor'>
            <h2 className='text-3xl'>Thông tin cơ bản</h2>
            <div className='AdminUserInfor_Content'>
                <div className='AdminUserInfor_Avatar'>
                    <Avatar src={user_infor_local.data.Anh} style={{ width: "200px", height: "200px" }} />
                    <div className='AdminUserInfor_Avatar_icon'><BsCamera className='iconBs' /></div>
                    <div className='AdminUserInfor_Avatar_title'>{user_infor_local.data.Ten} {user_infor_local.data.Ho}</div>
                </div>
                <div className='AdminUserInfor_Input'>
                    <Form name="form_item_path" layout="vertical" onFinish={onFinishUserInfor} onFinishFailed={onFinishFailed} style={{ maxWidth: 500 }} validateMessages={validateMessages}>
                        <Row>
                            <Col span={12}>
                                <MyFormItem name="Ho" label="Họ:" rules={[{ required: true }]} initialValue={user_infor_local.data.Ho}>
                                    <Input />
                                </MyFormItem>
                            </Col>
                            <Col span={12}>
                                <MyFormItem name="Ten" label="Tên:" rules={[{ required: true }]} initialValue={user_infor_local.data.Ten}>
                                    <Input />
                                </MyFormItem>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={12}>
                                <MyFormItem name="NgaySinh" label="Ngày sinh:" >
                                    <DatePicker
                                        defaultValue={dayjs(`${days}`, dayFormat)}
                                        format={dayFormat}
                                        style={{ width: "235px" }}
                                    />
                                </MyFormItem>
                            </Col>
                            <Col span={12}>
                                <MyFormItem name="SDT" label="Số điện thoại:" rules={[{ required: true }]} initialValue={user_infor_local.data.SDT}>
                                    <Input />
                                </MyFormItem>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <MyFormItem name="Email" label="Email:" initialValue={user_infor_local.data.Email}>
                                    <Input disabled />
                                </MyFormItem>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <MyFormItem name="TenDN" label="Tên đăng nhập:" initialValue={user_infor_local.data.TenDangNhap}>
                                    <Input disabled />
                                </MyFormItem>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={12}>
                                <MyFormItem name="PhanQuyen" label="Phân quyền:" initialValue={user_infor_local.data.PhanQuyen}>
                                    <Input disabled />
                                </MyFormItem>
                            </Col>
                        </Row>
                        {editModal ? (<>
                            <div className='AdminUserInfor_Input_btn'>
                                <button className='btn_Huy btn' onClick={() => {
                                    const userclick = document.querySelectorAll<HTMLElement>(".AdminUserInfor_Setting");
                                    userclick[0].style.display = "block";
                                    seteditModal(false);
                                }}>Huỷ</button>
                                <button className='btn_Luu btn'>Lưu</button>
                            </div>
                        </>) : (<></>)}
                    </Form>
                </div>
                <div className='AdminUserInfor_Setting' id='Setting'>
                    <div className='AdminUserInfor_Setting_item' id='Edit_UserInfor'>
                        <div className='AdminUserInfor_Setting_item_icon'><FiEdit className='Setting_icon' /></div>
                        <div className='AdminUserInfor_Setting_item_title'>Sữa thông </div>
                        <div className='AdminUserInfor_Setting_item_title'>tin</div>
                    </div>
                    <div className='AdminUserInfor_Setting_item' id='ResetPass_UserInfor'>
                        <div className='AdminUserInfor_Setting_item_icon'><FiLock className='Setting_icon' /></div>
                        <div className='AdminUserInfor_Setting_item_title'>Đổi </div>
                        <div className='AdminUserInfor_Setting_item_title'>mật khẩu</div>
                    </div>
                    <div className='AdminUserInfor_Setting_item' id='Logout_UserInfor'>
                        <div className='AdminUserInfor_Setting_item_icon'><FiLogOut className='Setting_icon' /></div>
                        <div className='AdminUserInfor_Setting_item_title'>Đăng xuất</div>
                    </div>
                </div>
            </div>
            {isModalOpen ? (
                <>
                    <div className='AdminUserInfor_Modal'>
                        <h4 style={{ textAlign: "center" }} className='text-2xl'>Thay đổi mật khẩu </h4>
                        <Form name="form_item_path" layout="vertical" onFinish={onFinishUserResetPass} onFinishFailed={onFinishFailed} style={{ maxWidth: 500 }} validateMessages={validateMessages}>
                            <MyFormItem name="passwordNow" label="Mật khẩu hiện tại:" rules={[{ required: true }]}>
                                <Input.Password />
                            </MyFormItem>
                            <MyFormItem name="passwordNew" label="Mật khẩu mới:" rules={[{ required: true }]}>
                                <Input.Password />
                            </MyFormItem>
                            <MyFormItem name="passwordNew2" label="Nhập lại mật khẩu mới:" rules={[{ required: true }]}>
                                <Input.Password />
                            </MyFormItem>
                            {error && (
                                <p style={{ marginLeft: "15px" }} className="text-danger">{error2}</p>
                            )}
                            <div className='AdminUserInfor_Modal_btn'>
                                <button className='btn_Huy btn' onClick={() => { handleCancel() }}>Huỷ</button>
                                <button className='btn_Luu btn'>Lưu</button>
                            </div>
                        </Form>
                    </div>
                </>
            ) : (
                <></>
            )}
            {isNotifi ? (
                <>
                    <div className='AdminUserInfor_Notification'>
                        <BsCheckCircle className='Icon_Notification' />
                        <div>Đổi mật khẩu thành công!</div>
                    </div>
                </>
            ) : (
                <></>
            )}
        </div>
    )
}

export default AdminUserInformation