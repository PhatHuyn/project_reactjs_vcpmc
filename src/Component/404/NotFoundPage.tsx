import React from 'react'

const NotFoundPage = () => {
    return (
        <div style={{ marginTop: "60px", color: "white" }}>
            <h4>Không tìm thấy trang web</h4>
            <p>Dường như đã có chút trục trặc hoặc link này đã hết hạn.</p>
        </div>
    )
}

export default NotFoundPage