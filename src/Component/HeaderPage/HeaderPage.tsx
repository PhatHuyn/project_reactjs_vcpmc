import React, { useEffect } from 'react'
import { DownOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Button, Dropdown, Space, Avatar } from 'antd';
import logoVN from '../../assets/images/logo_VN.png'
import logoEN from '../../assets/images/logo_English.png'
import { AppDispatch, RootState, useAppSelector } from '../../Redux/store';
import { useNavigate } from 'react-router';
import { AiOutlineDown } from 'react-icons/ai'
import { useDispatch } from 'react-redux';
import { setAdminMenu } from '../../Redux/AdminMenu/adminMenuSlice';

const HeaderPage = () => {
    const dispatch = useDispatch<AppDispatch>();
    const navigate = useNavigate();
    // data UserInfor Redux
    const user_infor = useAppSelector(
        (state: RootState) => state.userInfor.User_Information
    );

    const handleMenuClick: MenuProps['onClick'] = (e) => {
        console.log('click', e);
    };

    // UserClick avatar
    useEffect(() => {
        const userclick = document.querySelectorAll<HTMLElement>(".HeaderPage_UserManage_Img")
        userclick.forEach(user => {
            if (user != undefined) {
                user.addEventListener("click", function () {
                    navigate("/Admin/AdminUserInformation")
                    dispatch(setAdminMenu(false));
                })
            } else {
                return;
            }
        });
    }, [])
    useEffect(() => {
        const userclick = document.querySelectorAll<HTMLElement>(".HeaderPage_UserManage_Title")
        userclick.forEach(user => {
            if (user != undefined) {
                user.addEventListener("click", function () {
                    navigate("/Admin/AdminUserInformation")
                    dispatch(setAdminMenu(false));
                })
            } else {
                return;
            }
        });
    }, [])

    const items: MenuProps['items'] = [
        {
            label: "Tiếng Việt",
            key: '1',
        },
        {
            label: "English",
            key: '2',
        },
    ];
    const menuProps = {
        items,
        onClick: handleMenuClick,
    };
    const menuDropDown = () => {
        return (
            <Dropdown menu={menuProps}>
                <Button>
                    <Space>
                        Tiếng việt
                        <span className='Dropdown_logo' >
                            <img src={logoVN} alt="logo VN" />
                        </span>
                        <AiOutlineDown />
                    </Space>
                </Button>
            </Dropdown>
        )
    }

    return (
        <div className='HeaderPage'>
            <div className='HeaderPage_menuDropDown'>
                {menuDropDown()}
            </div>
            <div className='HeaderPage_UserManage'>
                <div className='btn HeaderPage_UserManage_Button'>
                    <div className='HeaderPage_UserManage_Img'>
                        <Avatar src={user_infor?.data.Anh} style={{ width: "50px", height: "50px", marginLeft: "-12px" }} />
                    </div>
                    <div style={{ color: "white" }} className='HeaderPage_UserManage_Title'>
                        <div>{user_infor?.data.Ho.slice(0, 2)}.{user_infor?.data.Ten}</div>
                        <div style={{ color: "#FF7506" }}>{user_infor?.data.PhanQuyen}</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HeaderPage