import React, { useEffect } from 'react'
import { RiStore3Line } from 'react-icons/ri'
import { MdPlaylistPlay } from 'react-icons/md'
import { FaRegCalendarAlt } from 'react-icons/fa'
import { ImFilePicture } from 'react-icons/im'
import { TbReportMoney } from 'react-icons/tb'
import { AiFillSetting, AiFillQuestionCircle } from 'react-icons/ai'
import { BiDotsVerticalRounded } from 'react-icons/bi'
import { Layout, theme, Breadcrumb } from 'antd';
import logo from '../../assets/images/vcpmc_logo.png'
import HeaderPage from './HeaderPage'
import { useNavigate } from 'react-router'
import { RootState, useAppSelector } from '../../Redux/store'
import { useDispatch } from 'react-redux'
import { AppDispatch } from '../../Redux/store'
import { setAdminMenu } from '../../Redux/AdminMenu/adminMenuSlice'

const { Content } = Layout;

interface HeaderMenu {
    Component: React.ReactElement
}

const HeaderMenuPage = (props: HeaderMenu) => {
    const dispatch = useDispatch<AppDispatch>();
    const navigate = useNavigate();

    useEffect(() => {
        handleOnClick()
    }, [])

    const handleOnClick = () => {
        let list = document.querySelectorAll<HTMLElement>(".HeaderLayoutMenu_List")
        for (let i = 0; i <= list.length; i++) {
            if (list[i] != undefined) {
                list[i].onclick = function () {
                    let j = 0;
                    while (j < list.length) {
                        list[j++].className = 'HeaderLayoutMenu_List';
                    }
                    list[i].className = 'HeaderLayoutMenu_List active';
                    let id = list[i].id;
                    navigate(id);
                    dispatch(setAdminMenu(true));
                }
            } else {
                return;
            }
        }
    }

    return (
        <div>
            <Layout style={{ minHeight: "100vh" }}>
                <div className='HeaderLayoutMenu'>
                    <ul>
                        <div className='HeaderLayoutMenu_imgLogo'>
                            <img src={logo} alt="logo" />
                        </div>
                        <li className='HeaderLayoutMenu_List active' id='/Admin/AdminRecordStore' >
                            <a href="#">
                                <div className='HeaderLayoutMenu_icon'><RiStore3Line style={{ fontSize: "25px", marginLeft: "75px", marginTop: "15px" }} /></div>
                                <div className='HeaderLayoutMenu_title'>
                                    <p>Kho bản ghi</p>
                                </div>
                            </a>
                        </li>
                        <li className='HeaderLayoutMenu_List' id='/playlist' >
                            <a href="#">
                                <div className='HeaderLayoutMenu_icon'><MdPlaylistPlay style={{ fontSize: "35px", marginLeft: "75px", marginTop: "10px" }} /></div>
                                <div className='HeaderLayoutMenu_title'>
                                    <p>Playlist</p>
                                </div>
                            </a>
                        </li>
                        <li className='HeaderLayoutMenu_List' id='/calendar' >
                            <a href="#">
                                <div className='HeaderLayoutMenu_icon'><FaRegCalendarAlt style={{ fontSize: "25px", marginLeft: "75px", marginTop: "15px" }} /></div>
                                <div className='HeaderLayoutMenu_title'>
                                    <p>Lập lịch phát</p>
                                </div>
                            </a>
                        </li>
                        <li className='HeaderLayoutMenu_List' id='/Admin/AdminContractManagement' >
                            <a href="#">
                                <div className='HeaderLayoutMenu_icon'><ImFilePicture style={{ fontSize: "25px", marginLeft: "75px", marginTop: "15px" }} /></div>
                                <div className='HeaderLayoutMenu_title'>
                                    <p>Quản lý</p>
                                    <div className='HeaderLayoutMenu_dot'><BiDotsVerticalRounded /></div>
                                </div>
                            </a>
                        </li>
                        <li className='HeaderLayoutMenu_List' id='/revenue' >
                            <a href="#">
                                <div className='HeaderLayoutMenu_icon'><TbReportMoney style={{ fontSize: "35px", marginLeft: "70px", marginTop: "10px" }} /></div>
                                <div className='HeaderLayoutMenu_title'>
                                    <p>Doanh thu</p>
                                    <div className='HeaderLayoutMenu_dot'><BiDotsVerticalRounded /></div>
                                </div>
                            </a>
                        </li>
                        <li className='HeaderLayoutMenu_List' id='/setting' >
                            <a href="#">
                                <div className='HeaderLayoutMenu_icon'><AiFillSetting style={{ fontSize: "25px", marginLeft: "75px", marginTop: "15px" }} /></div>
                                <div className='HeaderLayoutMenu_title'>
                                    <p>Cài đặt</p>
                                    <div className='HeaderLayoutMenu_dot'><BiDotsVerticalRounded /></div>
                                </div>
                            </a>
                        </li>
                        <li className='HeaderLayoutMenu_List' id='/support' >
                            <a href="#">
                                <div className='HeaderLayoutMenu_icon'><AiFillQuestionCircle style={{ fontSize: "25px", marginLeft: "75px", marginTop: "15px" }} /></div>
                                <div className='HeaderLayoutMenu_title'>
                                    <p>Hỗ trợ</p>
                                    <div className='HeaderLayoutMenu_dot'><BiDotsVerticalRounded /></div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <Layout style={{ marginLeft: "170px" }}>
                    <HeaderPage />
                    <Content>
                        <div>{props.Component}</div>
                    </Content>
                </Layout>
            </Layout >
        </div>
    )
}

export default HeaderMenuPage