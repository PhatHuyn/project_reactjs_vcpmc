import React, { useState } from 'react'
import { Button, Form, Input } from 'antd';
import type { FormItemProps } from 'antd';
import { confirmPasswordReset } from 'firebase/auth';
import { auth } from '../../Firebase/init_Firebase';
import { useLocation, useNavigate } from 'react-router-dom'
import logo from '../../assets/images/vcpmc_logo.png'

function useQuery() {
    const location = useLocation()
    return new URLSearchParams(location.search);
}

const ResetPassPage = () => {
    const [errorpass, seterrorpass] = useState(false)
    const [errorpass2, seterrorpass2] = useState("")
    const query = useQuery()
    const queryoobCode: any = query.get('oobCode')
    let navigate = useNavigate();
    const MyFormItemContext = React.createContext<(string | number)[]>([]);
    function toArr(str: string | number | (string | number)[]): (string | number)[] {
        return Array.isArray(str) ? str : [str];
    }
    const MyFormItem = ({ name, ...props }: FormItemProps) => {
        const prefixPath = React.useContext(MyFormItemContext);
        const concatName = name !== undefined ? [...prefixPath, ...toArr(name)] : undefined;

        return <Form.Item name={concatName} {...props} />;
    };
    const validateMessages = {
        required: '${label} không được bỏ trống!',
        types: {
            email: '${label} phải là dạng email',
            number: '${label} is not a valid number!',
        },
        number: {
            range: '${label} must be between ${min} and ${max}',
        },
    };


    const onFinish = (values: any) => {
        console.log(values);
        // Reset lại pass
        const pass1 = values.password1;
        const pass2 = values.password2;
        if (pass1 === pass2) {
            confirmPasswordReset(auth, queryoobCode, pass1).then((res) => {
                alert("Chúc mừng bạn đổi mk thành công!!!")
                navigate("/login");
            }).catch((err) => {
                console.log("errResetPass", err.message);
            })
        } else {
            seterrorpass(true);
            seterrorpass2("Mật khẩu phải đặt giống nhau");
        }
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };


    return (
        <div className='LoginPage'>
            <div className='LoginPageImg'>
                <img src={logo} alt="" />
            </div>
            <h2 className='text-5xl'>Đặt lại mật khẩu</h2>
            <div className='LoginForm'>
                <Form name="form_item_path" layout="vertical" onFinish={onFinish} onFinishFailed={onFinishFailed} style={{ maxWidth: 400 }} validateMessages={validateMessages}>
                    <MyFormItem name="password1" label="Mật khẩu mới" rules={[{ required: true, message: 'Vui lòng nhập mật khẩu!' }]}>
                        <Input.Password />
                    </MyFormItem>
                    <MyFormItem name="password2" label="Nhập lại mật khẩu mới" rules={[{ required: true, message: 'Vui lòng nhập mật khẩu mới!' }]}>
                        <Input.Password />
                    </MyFormItem>
                    {errorpass && (
                        <p className="text-danger">{errorpass2}</p>
                    )}
                    <br />

                    <Button type="primary" htmlType="submit" style={{ background: "#FF7506", width: "150px" }}>
                        Xác nhận
                    </Button>
                </Form>
            </div>
            <div style={{ marginTop: "8%" }}>
                <a href="/Login" style={{ color: "#FF7506" }}>Quay lại đăng nhập</a>
            </div>
        </div>
    )
}

export default ResetPassPage
