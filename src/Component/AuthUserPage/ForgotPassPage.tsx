import React, { useState } from 'react'
import { Button, Form, Input } from 'antd';
import type { FormItemProps } from 'antd';
import { sendPasswordResetEmail } from 'firebase/auth';
import { auth } from '../../Firebase/init_Firebase';
import logo from '../../assets/images/vcpmc_logo.png'

const ForgotPassPage = () => {
    // Hiển thị lỗi khi user nhập sai
    const [erroremail, seterroremail] = useState(false)
    const [erroremail2, seterroremail2] = useState("")
    const [email, setemail] = useState(false)

    const MyFormItemContext = React.createContext<(string | number)[]>([]);
    function toArr(str: string | number | (string | number)[]): (string | number)[] {
        return Array.isArray(str) ? str : [str];
    }
    const MyFormItem = ({ name, ...props }: FormItemProps) => {
        const prefixPath = React.useContext(MyFormItemContext);
        const concatName = name !== undefined ? [...prefixPath, ...toArr(name)] : undefined;

        return <Form.Item name={concatName} {...props} />;
    };
    const validateMessages = {
        required: '${label} không được bỏ trống!',
        types: {
            email: '${label} phải là dạng email',
            number: '${label} is not a valid number!',
        },
        number: {
            range: '${label} must be between ${min} and ${max}',
        },
    };

    const onFinish = (values: any) => {
        console.log(values);
        const email = values.eamil
        // gửi lại email
        sendPasswordResetEmail(auth, email, { url: "http://localhost:3000/login" }).then((res) => {
            console.log("res", res);
            setemail(true);
        }).catch((err) => {
            console.log("err", err.message);
            seterroremail(true);
            seterroremail2("Email có thể chưa đăng ký hoặc bạn nhập sai email!");
        })
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className='LoginPage'>
            <div className='LoginPageImg'>
                <img src={logo} alt="" />
            </div>
            <h2 className='text-5xl'>Khôi phục mật khẩu</h2>
            <p>Vui lòng nhập địa chỉ email đã đăng ký để yêu cầu khôi phục mật khẩu</p>
            {email ? (
                <div className='LoginForm'>
                    <div>Click vào đường link được đính kèm trong email để chuyển đến trang đặt mật khẩu</div>
                </div>
            ) : (
                <div className='LoginForm'>
                    <div style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
                        <div style={{ width: "400px" }}>
                            <Form name="form_item_path" layout="vertical" onFinish={onFinish} onFinishFailed={onFinishFailed} style={{ maxWidth: 400 }} validateMessages={validateMessages}>
                                <MyFormItem name="eamil" label="Email" rules={[{ required: true, type: 'email' }]}>
                                    <Input name='email' />
                                </MyFormItem>
                                {erroremail && (
                                    <p className="text-danger">{erroremail2}</p>
                                )}
                                <br />

                                <Button type="primary" htmlType="submit" style={{ background: "#FF7506", width: "150px" }}>
                                    Xác nhận
                                </Button>
                            </Form>
                        </div>
                    </div>
                </div>
            )
            }
            <div style={{ marginTop: "8%" }}>
                <a href="/Login" style={{ color: "#FF7506" }}>Quay lại đăng nhập</a>
            </div>
        </div >
    )
}

export default ForgotPassPage