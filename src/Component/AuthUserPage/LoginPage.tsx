import React, { useState } from 'react'
import { Button, Checkbox, Form, Input } from 'antd';
import type { FormItemProps } from 'antd';
import { useAppDispatch } from '../../Redux/store';
import { localServ } from '../../Services/localService';
import { useNavigate } from "react-router-dom";
import logo from '../../assets/images/vcpmc_logo.png'
import { valuesSubmit } from '../../Interface/User/userAuth';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth } from '../../Firebase/init_Firebase';
import { UserLogin } from '../../Redux/User/userAuthSlice';

const LoginPage: React.FC = () => {
    const dispatch = useAppDispatch();
    let navigate = useNavigate();
    // Dữ liệu từ Local
    const User = localServ.UserRemember.get();
    const remember = User?.remember;
    const name = User?.username;
    const pass = User?.password;
    // Hiển thị lỗi khi user nhập sai
    const [error, seterror] = useState(false)
    const [error2, seterror2] = useState("")

    //Thư viện hỗ trợ
    const MyFormItemContext = React.createContext<(string | number)[]>([]);
    function toArr(str: string | number | (string | number)[]): (string | number)[] {
        return Array.isArray(str) ? str : [str];
    }
    const MyFormItem = ({ name, ...props }: FormItemProps) => {
        const prefixPath = React.useContext(MyFormItemContext);
        const concatName = name !== undefined ? [...prefixPath, ...toArr(name)] : undefined;
        return <Form.Item name={concatName} {...props} />;
    };
    const validateMessages = {
        required: '${label} không được bỏ trống!',
        types: {
            email: '${label} phải là dạng email',
            number: '${label} is not a valid number!',
        },
        number: {
            range: '${label} must be between ${min} and ${max}',
        },
    };

    // Đăng nhập
    const SignIn = (username: string, password: string, remember: boolean) => {
        signInWithEmailAndPassword(auth, username, password).then((res) => {
            const userAuth_login = { username, password, remember }
            // lấy dữ liệu user login
            const user = res.user;
            alert("Đăng nhập thành công!!!");
            localServ.User.set(user);
            localServ.UserLogin.set(userAuth_login);
            // Lưu remember
            if (remember != undefined && remember == true) {
                localServ.UserRemember.set(userAuth_login)
                dispatch(UserLogin(userAuth_login));
            } else if (remember == undefined) {
                const userAuth_login2 = { username, password };
                dispatch(UserLogin(userAuth_login2));
            } else if (remember == false) {
                localServ.UserRemember.remove();
                dispatch(UserLogin(userAuth_login));
            }
            navigate("/Admin/AdminRecordStore");
        }).catch((error) => {
            const errcode = error.code;
            const errorMessage = error.message;
            console.log(errcode + errorMessage);
            seterror(true);
            seterror2("Sai Tên đăng nhập hoặc password");
        })
    }

    const onFinish = (values: valuesSubmit) => {
        const username = values.username;
        const password = values.password;
        const remember = values.remember;
        SignIn(username, password, remember);

    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className='LoginPage'>
            <div className='LoginPageImg'>
                <img src={logo} alt="" />
            </div>
            <h2 className='text-5xl'>Đăng nhập</h2>

            <div className='LoginForm'>
                <Form name="form_item_path" layout="vertical" onFinish={onFinish} onFinishFailed={onFinishFailed} style={{ maxWidth: 400 }} validateMessages={validateMessages}>
                    <MyFormItem name="username" label="Tên đăng nhập" rules={[{ required: true, type: 'email' }]} initialValue={name}>
                        <Input />
                    </MyFormItem>
                    <MyFormItem name="password" label="Password" rules={[{ required: true, message: 'Vui lòng nhập mật khẩu!' }]} initialValue={pass}>
                        <Input.Password />
                    </MyFormItem>
                    {error && (
                        <p style={{ marginLeft: "15px" }} className="text-danger">{error2}</p>
                    )}
                    <div style={{ marginRight: "65%", marginTop: "-10px", width: "160px" }}>
                        <Form.Item name="remember" valuePropName="checked"  >
                            <Checkbox defaultChecked={remember}>Ghi nhớ đăng nhập</Checkbox>
                        </Form.Item>
                    </div>
                    <br />

                    <Button type="primary" htmlType="submit" style={{ background: "#FF7506", width: "150px" }}>
                        Đăng Nhập
                    </Button>
                </Form>
            </div>

            <div style={{ marginTop: "6%" }}>
                <a href="/forgotPass" style={{ color: "#FF7506" }}>Quên mật khẩu?</a>
            </div>
        </div >
    )
}

export default LoginPage
