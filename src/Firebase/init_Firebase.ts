import { initializeApp } from "firebase/app";
import {getAuth} from 'firebase/auth'
import { collection, getFirestore } from "firebase/firestore";

const firebaseConfig = { 
  apiKey : "AIzaSyAu56ciOYdrg1HoiSbZ_KmxA5lnte6nio8" , 
  authDomain : "project-vcpmc.firebaseapp.com" , 
  projectId : "project-vcpmc" , 
  storageBucket : "project-vcpmc.appspot.com" , 
  messagingSenderId : "608618384621" , 
  appId : "1:608618384621:web:e1f23421e548c6e6c5d0e4" 
};

// Initialize Firebase
 const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth(app);
export const userInforCollectionRef = collection(db, "UserInformation");  
export const userAuthCollectionRef = collection(db, "UserAuth");